<?php

class Core_Dto_Exception_EntityNotSet extends Core_Dto_Exception
{
    protected $message = 'DTO Error: entity not set on specified options.';
}