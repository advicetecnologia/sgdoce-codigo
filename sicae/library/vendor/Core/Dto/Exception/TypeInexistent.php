<?php

class Core_Dto_Exception_TypeInexistent extends Core_Dto_Exception
{
    protected $message = 'DTO Error: specified type of DTO does not exist.';
}