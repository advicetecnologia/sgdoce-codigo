<?php

class Core_Dto_Exception extends Core_Exception
{
    protected $message = 'DTO Error: non specified error.';
}