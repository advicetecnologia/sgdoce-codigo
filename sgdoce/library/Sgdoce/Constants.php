<?php

/**
 * Copyright 2012 do ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro
 * dos termos da Licença Pública Geral GNU como publicada pela Fundação do Software Livre
 * (FSF); na versão 2 da Licença.
 * Este programa é distribuído na esperança que possa ser  útil, mas SEM NENHUMA GARANTIA;
 * sem uma garantia implícita de ADEQUAÇÃO a qualquer  MERCADO ou APLICAÇÃO EM PARTICULAR.
 * Veja a Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no
 * endereço www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF) Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 */

/**
 * Description of Constants
 * @author André Luiz Geraldi <andre.geraldi@ctis.com.br>
 */
final class Sgdoce_Constants
{
    // NACIONALIDADE
    const NACIONALIDADE_BRASILEIRA   = 1;
    const NACIONALIDADE_ESTRANGEIRA  = 2;

    const PESSOA_CORPORATIVO    = 1;
    const PESSOA_SGDOCE         = 2;

    // TIPO DOCUMENTO
    const ATRIBUTO_TIPO_DOCUMENTO     = 29;

    // procedencia
    const PROCEDENCIA_INTERNA = 1;
    const PROCEDENCIA_EXTERNA = 3;

    // tipo de informação
    const INFO_AVANCADA = 3;
}