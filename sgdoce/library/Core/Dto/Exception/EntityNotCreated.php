<?php

class Core_Dto_Exception_EntityNotCreated extends Core_Dto_Exception
{
    protected $message = 'DTO Error: specified entity could not be created.';
}