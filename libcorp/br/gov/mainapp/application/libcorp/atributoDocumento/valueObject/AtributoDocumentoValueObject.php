<?php
/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\atributoDocumento\valueObject;
use br\gov\sial\core\valueObject\ValueObjectAbstract as ParentValueObject;

/**
  * SISICMBio
  *
  * Generated By SIAL Generator - vs 0.2.0
  *
  * @name AtributoDocumentoValueObject
  * @package br.gov.icmbio.sisicmbio.application.libcorp.atributoDocumento
  * @subpackage valueObject
  * @schema(name="corporativo")
  * @entity(name="atributo_documento")
  * @author Fabio Lima <fabioolima@gmail.com>
  * @author J. Augusto <augustowebd@gmail.com>
  * @version $Id$
  * @log(name="all")
  * */
class AtributoDocumentoValueObject extends ParentValueObject
{
    /**
     * @attr (
     *  name="noAtributoDocumento",
     *  database="no_atributo_documento",
     *  type="string",
     *  nullable="FALSE",
     *  get="getNoAtributoDocumento",
     *  set="setNoAtributoDocumento"
     * )
     * */
     private $_noAtributoDocumento;

    /**
     * @attr (
     *  name="sqAtributoDocumento",
     *  database="sq_atributo_documento",
     *  primaryKey="TRUE",
     *  type="integer",
     *  nullable="FALSE",
     *  get="getSqAtributoDocumento",
     *  set="setSqAtributoDocumento"
     * )
     * */
     private $_sqAtributoDocumento;

    /**
     * @param string $noAtributoDocumento
     * @param integer $sqAtributoDocumento
     * */
    public function __construct ($noAtributoDocumento = NULL,
                                 $sqAtributoDocumento = NULL)
    {
        parent::__construct();
        $this->setNoAtributoDocumento($noAtributoDocumento)
             ->setSqAtributoDocumento($sqAtributoDocumento)
             ;
    }

    /**
     * @return string
     * */
    public function getNoAtributoDocumento ()
    {
        return $this->_noAtributoDocumento;
    }

    /**
     * @return integer
     * */
    public function getSqAtributoDocumento ()
    {
        return $this->_sqAtributoDocumento;
    }

    /**
     * @param string $noAtributoDocumento
     * @return AtributoDocumentoValueObject
     * */
    public function setNoAtributoDocumento ($noAtributoDocumento = NULL)
    {
        $this->_noAtributoDocumento = $noAtributoDocumento;
        return $this;
    }

    /**
     * @param integer $sqAtributoDocumento
     * @return AtributoDocumentoValueObject
     * */
    public function setSqAtributoDocumento ($sqAtributoDocumento = NULL)
    {
        $this->_sqAtributoDocumento = $sqAtributoDocumento;
        return $this;
    }
}