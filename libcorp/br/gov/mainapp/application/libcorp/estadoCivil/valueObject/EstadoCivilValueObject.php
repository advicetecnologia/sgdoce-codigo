<?php
/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\estadoCivil\valueObject;
use br\gov\sial\core\valueObject\ValueObjectAbstract as ParentValueObject;

/**
  * SISICMBio
  *
  * Generated By SIAL Generator - vs 0.2.0
  *
  * @name EstadoCivilValueObject
  * @package br.gov.mainapp.application.libcorp.estadoCivil
  * @subpackage valueObject
  * @schema(name="corporativo")
  * @entity(name="estado_civil")
  * @author Fabio Lima <fabioolima@gmail.com>
  * @author J. Augusto <augustowebd@gmail.com>
  * @version $Id$
  * @log(name="all")
  * */
class EstadoCivilValueObject extends ParentValueObject
{
    /**
     * @attr (
     *  name="noEstadoCivil",
     *  database="no_estado_civil",
     *  type="string",
     *  nullable="FALSE",
     *  get="getNoEstadoCivil",
     *  set="setNoEstadoCivil"
     * )
     * */
     private $_noEstadoCivil;

    /**
     * @attr (
     *  name="sqEstadoCivil",
     *  database="sq_estado_civil",
     *  primaryKey="TRUE",
     *  type="integer",
     *  nullable="FALSE",
     *  get="getSqEstadoCivil",
     *  set="setSqEstadoCivil"
     * )
     * */
     private $_sqEstadoCivil;

    /**
     * @param string $noEstadoCivil
     * @param integer $sqEstadoCivil
     * */
    public function __construct ($noEstadoCivil = NULL,
                                 $sqEstadoCivil = NULL)
    {
        parent::__construct();
        $this->setNoEstadoCivil($noEstadoCivil)
             ->setSqEstadoCivil($sqEstadoCivil)
             ;
    }

    /**
     * @return string
     * */
    public function getNoEstadoCivil ()
    {
        return $this->_noEstadoCivil;
    }

    /**
     * @return integer
     * */
    public function getSqEstadoCivil ()
    {
        return $this->_sqEstadoCivil;
    }

    /**
     * @param string $noEstadoCivil
     * @return EstadoCivilValueObject
     * */
    public function setNoEstadoCivil ($noEstadoCivil = NULL)
    {
        $this->_noEstadoCivil = $noEstadoCivil;
        return $this;
    }

    /**
     * @param integer $sqEstadoCivil
     * @return EstadoCivilValueObject
     * */
    public function setSqEstadoCivil ($sqEstadoCivil = NULL)
    {
        $this->_sqEstadoCivil = $sqEstadoCivil;
        return $this;
    }
}