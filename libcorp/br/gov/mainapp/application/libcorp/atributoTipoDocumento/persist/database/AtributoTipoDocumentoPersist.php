<?php
/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\atributoTipoDocumento\persist\database;
use br\gov\sial\core\persist\exception\PersistException,
    br\gov\sial\core\persist\database\Persist as ParentPersist,
    br\gov\mainapp\application\libcorp\atributoDocumento\valueObject\AtributoDocumentoValueObject,
    br\gov\mainapp\application\libcorp\atributoTipoDocumento\valueObject\AtributoTipoDocumentoValueObject;

/**
  * SISICMBio
  *
  * Generated By SIAL Generator - vs 0.2.0
  *
  * @name AtributoTipoDocumentoPersist
  * @package br.gov.icmbio.sisicmbio.application.libcorp.atributoTipoDocumento.persist
  * @subpackage database
  * @author Fabio Lima <fabioolima@gmail.com>
  * @author J. Augusto <augustowebd@gmail.com>
  * @version $Id$
  * */
class AtributoTipoDocumentoPersist extends ParentPersist
{
    public function findByTipo (AtributoTipoDocumentoValueObject $voAttrTipoDocumento)
    {
        try {
            /*
             * SELECT
             *     atd.sq_atributo_documento,
             *     AD.no_atributo_documento
             * FROM
             *      corporativo.atributo_tipo_documento ATD
             * JOIN corporativo.atributo_documento AD ON AD.sq_atributo_documento = ATD.sq_atributo_documento
             * WHERE ATD.sq_tipo_documento = :sq_tipo_documento;
             */
            # obtem a entidade com base na anotacao
            $eAttrTipoDoc = $this->getEntity(array('atd' => $this->annotation()->load()->class));

            # cria entidade bioma atributo documento
            $eAttrDocum   = $this->getEntity(array('ad' => AtributoDocumentoValueObject::factory()));

            # cria objeto de consulta baseando-se em AttrTipoDoc
            $query  = $this->getQuery($eAttrTipoDoc)

                # efetua join de bioma com biomaMunicipio
                ->join($eAttrDocum, $eAttrTipoDoc->column('sqAtributoDocumento')->equals($eAttrDocum->column('sqAtributoDocumento')))

                # aplica filtro
                ->where($eAttrTipoDoc->column('sqAtributoDocumento')->equals($voAttrTipoDocumento->getSqAtributoTipoDocumento()))
            ;

            # executa query
            return $this->execute($query);
        } catch (\Exception $excp) {
            throw new PersistException($excp->getMessage());
        }
    }
}